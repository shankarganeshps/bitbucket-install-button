# Bitbucket add-on install button

This JavaScript library is an easy way to embed an install button or link for
your Bitbucket add-on on an external marketing site, documentation page, or
blog.

See [the documentation](https://bitbucket-install-button.aerobatic.io) for more details.