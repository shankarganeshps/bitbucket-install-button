window.BitbucketAddOnInstallHandler = {};

(function(BitbucketAddOnInstallHandler) {

  /**
   * @param {Object} opts configuration options
   * @param {String} opts.buttonClass the class used to look up your add-on install button(s)
   * @param {String} opts.descriptorUri the URI of your descriptor (defaults to the current directory + '/connect.json')
   * @param {String} opts.redirectUri the URI of to redirect to, MUST start with your add-ons baseUrl (defaults to the current page)
   */
  BitbucketAddOnInstallHandler.bindInstallButtons = function(opts) {

    opts = parseOpts(opts);

    $(opts.selector).attr('href', 'https://bitbucket.org/site/addons/authorize' +
        '?descriptor_uri=' + opts.descriptorUri +
        '&redirect_uri=' + opts.redirectUri);

    function parseOpts(input) {
      input = input || {};
      var opts = {};

      opts.selector = '.' + (input.buttonClass || 'bitbucket-install-button');

      var domainUrl = 'https://' + window.location.host; // no trailing slash
      var pageUrl = domainUrl + window.location.pathname;

      var dirUrl = pageUrl;
      var lastSlash = dirUrl.lastIndexOf('/');
      if (lastSlash > -1 && lastSlash !== dirUrl.length - 1) {
        dirUrl = dirUrl.substring(0, dirUrl.lastIndexOf('/') + 1);
      }

      if (input.descriptorUri) {
        opts.descriptorUri = parseUrlOpt(input.descriptorUri, domainUrl, dirUrl);
      } else {
        opts.descriptorUri = dirUrl + 'connect.json';
      }

      if (input.redirectUri) {
        opts.redirectUri = parseUrlOpt(input.redirectUri, domainUrl, dirUrl);
      } else {
        opts.redirectUri = pageUrl;
      }

      ['descriptorUri', 'redirectUri'].forEach(function(prop) {
        var shouldBeString = opts[prop];
        if (typeof shouldBeString !== 'string') {
          throw 'opts.' + prop + ' should be a string, not ' + typeof shouldBeString;
        }
      });

      return opts;
    }
  };

  function htmlEncode(value){
    return $('<div/>').text(value).html();
  }

  function parseUrlOpt(optUrl, domainUrl, dirUrl) {
    if (optUrl.indexOf("http") === 0) {
      // absolute URL passed in
      return optUrl;
    } else if (optUrl.indexOf("/") === 0) {
      // relative to root URI passed in
      return domainUrl + optUrl;
    } else {
      // relative to page URI passed in
      return dirUrl + optUrl;
    }
  }

  /**
   * @param {Object} opts configuration options
   * @param {String} opts.showMessage whether to display an AUI message on installation
   * success/failure (defaults to true)
   * @param {String} opts.messageContext selector of the element to append AUI messages
   * to (defaults to #aui-message-bar)
   * @param {Function} opts.success called on redirect after an installation success
   * @param {Function} opts.error called on redirect after an installation error
   * @param {Function} opts.always called on redirect regardless of success or error
   * @param {Object|Function|String} opts.successMessage options for the success AUI
   * message (defaults to a generic success message). If passed a Function/String it
   * will be used as opts.successMessage.body
   * @param {Function|String} opts.successMessage.title the success AUI message title
   * (defaults to a generic success message). Functions will be invoked with a params
   * object that MAY contain html encoded clientKey, username and/or type properties.
   * @param {Function|String} opts.successMessage.body the success AUI message body
   * (defaults to a generic success message). Functions will be handled in a similar
   * fashion to opts.successMessage.body
   * @param {Object|Function|String} opts.errorMessage options for the error AUI
   * message (defaults to a generic error message). If passed a Function/String it
   * will be used as opts.errorMessage.body
   * @param {Function|String} opts.errorMessage.title the error AUI message title
   * (defaults to a generic error message). Functions will be invoked with a params
   * object that MAY contain html encoded error and error_description properties.
   * @param {Function|String} opts.errorMessage.body the error AUI message body
   * (defaults to a generic error message). Functions will be handled in a similar
   * fashion to opts.errorMessage.body
   */
  BitbucketAddOnInstallHandler.handlePostInstallRedirect = function(opts) {

    opts = parseOpts(opts);

    if (document.readyState == 'complete' || document.readyState == 'loaded') {
      onPageContentLoaded();
    } else {
      document.addEventListener('DOMContentLoaded', onPageContentLoaded);
    }

    function onPageContentLoaded() {
      // parse & html encode query params
      var params = {};
      ['clientKey', 'error', 'error_description', 'username', 'type'].forEach(function (name) {
        var value = queryParam(name);
        if (value) {
          params[name] = htmlEncode(value);
        }
      });

      // installation succeeded
      if (params.clientKey || params.username) {
        opts.success();
        opts.always();
        if (opts.showMessage) {
          AJS.messages.success(opts.messageContext, {
            title: opts.successMessage.title(params),
            body: opts.successMessage.body(params)
          }).show();
        }
      }
      // installation failed
      else if (params.error) {
        opts.error();
        opts.always();
        if (opts.showMessage) {
          AJS.messages.error(opts.messageContext, {
            title: opts.errorMessage.title(params),
            body: opts.errorMessage.body(params)
          }).show();
        }
      }
    }

    function parseOpts(input) {
      input = input || {};
      var opts = {};

      opts.showMessage = input.showMessage !== undefined ? input.showMessage : input.successMessage || input.errorMessage;
      opts.messageContext = input.messageContext || '#aui-message-bar';

      ['success', 'error', 'always'].forEach(function(prop) {
        var shouldBeFn = input[prop];
        if (!shouldBeFn) {
          opts[prop] = function() {/* noop */};
        } else if (typeof shouldBeFn !== 'function') {
          throw 'opts.' + prop + ' should be a function, not ' + typeof shouldBeFn;
        } else {
          opts[prop] = input[prop];
        }
      });

      opts.successMessage = buildMessageOpts(
        input.successMessage,
        'Installation success!',
        'The add-on was installed successfully.'
      );

      opts.errorMessage = buildMessageOpts(
        input.errorMessage,
        'Installation error',
        function(error, errorDescription) {
          return 'Installation failed: ' +
            htmlEncode(error || 'unknown') +
            ' (' + htmlEncode(errorDescription) + ')';
        }
      );

      return opts;
    }

    function buildMessageOpts(input, defaultTitle, defaultBody) {
      var messageOpts = {};
      if (typeof input === 'string' || typeof input === 'function') {
        messageOpts.body = input;
      } else if (typeof input === 'object') {
        messageOpts.title = input.title;
        messageOpts.body = input.body;
      }
      messageOpts.title = messageOpts.title || defaultTitle;
      messageOpts.body = messageOpts.body || defaultBody;
      buildFormatterFunction(messageOpts, 'title', messageOpts.title);
      buildFormatterFunction(messageOpts, 'body', messageOpts.body);
      return messageOpts;
    }

    function buildFormatterFunction(opts, property, value) {
      switch (typeof value) {
        case 'function':
          // already a function, sweet
          break;
        case 'string':
          opts[property] = function() {
            return value;
          };
          break;
        default:
          throw property + ' must be a string or function, not a ' + typeof value;
      }
    }

    function queryParam(name) {
      var matches = location.search.match(new RegExp('[\?&]' + name + '=([^\&]*)'));
      return matches && decodeURIComponent(matches[1].replace(/\+/g, ' '));
    }

  };

})(window.BitbucketAddOnInstallHandler);